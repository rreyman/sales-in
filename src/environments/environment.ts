// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: 'AIzaSyAvpD23ciQWoxcsXzeCHMqVxcXrUXp5Yy0',
  authDomain: 'salesin-2dc97.firebaseapp.com',
  databaseURL: 'https://salesin-2dc97.firebaseio.com',
  projectId: 'salesin-2dc97',
  storageBucket: 'salesin-2dc97.appspot.com',
  messagingSenderId: '774736707587',
  appId: '1:774736707587:web:7c1d43876132c87db14e36'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
