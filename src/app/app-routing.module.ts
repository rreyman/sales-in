import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { NologinGuard } from './guards/nologin.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login.module').then( m => m.LoginPageModule), canActivate : [NologinGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./components/register/register.module').then( m => m.RegisterPageModule), canActivate : [NologinGuard]
  },
  {
    path: 'users-tabs',
    loadChildren: () => import('./components/users-tabs/users-tabs.module').then( m => m.UsersTabsPageModule), canActivate : [AuthGuard]
  },
  {
    path: 'user-offers',
    loadChildren: () => import('./components/user-offers/user-offers.module').then( m => m.UserOffersPageModule), canActivate : [AuthGuard]
  },
  {
    path: 'user-applies',
    loadChildren: () => import('./components/user-applies/user-applies.module').then( m => m.UserAppliesPageModule), canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
