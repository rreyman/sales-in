export interface Offers {
    ciclo: string;
    descripcion: string;
    fechamax: Date;
    numcandidatos: number;
    requisitos: string[];
    titular: string;
    id: string; 	// Aquí guardaremos el id del documento para poder referenciarlo
}
