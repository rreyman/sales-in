export interface Users {
    actived: boolean;
    cicle: string;
    dni: string;
    email: string;
    id: string;
    name: string;
    password: string;
    surname: string;
    type: string;
}
