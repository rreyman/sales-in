import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Offers } from '../models/offers';
import { Users } from '../models/users';
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  newUser: Users;
  Offers: Offers[] = [];

  constructor(private AFauth: AngularFireAuth,
              private router: Router,
              private db: AngularFirestore) { }

  login(email: string, password: string) {
      return new Promise((resolve, rejected) => {
        this.AFauth.signInWithEmailAndPassword(email, password)
            .then(user => {
              resolve(user);
            }).catch(err => {
              rejected(err);
            });
      });
  }

  logout() {
    this.AFauth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

  activate(user: Users) {
    // return new Promise((resolve, rejected) => {
    //   this.AFauth.createUserWithEmailAndPassword(user.email, user.password)
    //     .then(res => {
    //       // Una vez creado el usuario lo añadimos en la tabla users
    //       const uid = res.user.uid;
    //       this.db.collection('users').doc(uid).set({
    //         actived: false,
    //         cicle: user.cicle,
    //         dni: user.dni,
    //         id: uid,
    //         email: user.email,
    //         name: user.name,
    //         password: user.password,
    //         surname: user.surname,
    //         type: 'user'
    //       });
    //       resolve(res);
    //     }).catch(err => {
    //       rejected(err);
    //     });
    //   });
  }

  registerNewUser(user: Users) {
    return new Promise((resolve, rejected) => {
      this.AFauth.createUserWithEmailAndPassword(user.email, user.password)
        .then(res => {
          // Una vez creado el usuario lo añadimos en la tabla users
          const uid = res.user.uid;
          user.id = uid;
          console.log(user);
          this.db.collection('users').doc(uid).set(user)
            .then(() => this.logout());
          resolve(res);
        }).catch(err => {
          rejected(err);
        });
      });
  }
}
