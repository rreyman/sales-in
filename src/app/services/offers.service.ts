import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Offers } from '../models/offers';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  constructor(private db: AngularFirestore) { }

  getOfertas() {
    return this.db.collection('ofertas').snapshotChanges().pipe(map(of => {
      return of.map(ofert => {
        const data = ofert.payload.doc.data() as Offers;
        data.id = ofert.payload.doc.id;
        return data;
      });
    }));
  }
}
