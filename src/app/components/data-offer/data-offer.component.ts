import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';


@Component({
  selector: 'app-data-offer',
  templateUrl: './data-offer.component.html',
  styleUrls: ['./data-offer.component.scss'],
})
export class DataOfferComponent implements OnInit {
  ciclo: string;
  descripcion: string;
  fechamax: Date;
  numcandidatos: number;
  requisitos: string[];
  titular: string;
  id: string;

  constructor(private navParams: NavParams,
              private modalCtrl: ModalController) { }

  ngOnInit() {
    this.ciclo = this.navParams.get('ciclo');
    this.descripcion = this.navParams.get('descripcion');
    this.fechamax = this.navParams.get('fechamax');
    this.numcandidatos = this.navParams.get('numcandidatos');
    this.requisitos = this.navParams.get('requisitos');
    this.titular = this.navParams.get('titular');
    this.id = this.navParams.get('id');
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }
}
