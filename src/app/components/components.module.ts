import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfertasComponent } from './ofertas/ofertas.component';
import { OfertaComponent } from './oferta/oferta.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    OfertasComponent,
    OfertaComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    OfertasComponent,
    OfertaComponent
  ]
})
export class ComponentsModule { }
