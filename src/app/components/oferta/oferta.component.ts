import { Component, OnInit, Input } from '@angular/core';
import { Offers } from '../../models/offers';
import { ModalController } from '@ionic/angular';
import { DataOfferComponent } from '../data-offer/data-offer.component';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.scss'],
})
export class OfertaComponent implements OnInit {
  @Input() oferta: Offers;
  @Input() i: number;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  viewOffer(oferta: Offers) {
    console.log(oferta);
    this.modalCtrl.create({
      component: DataOfferComponent,
      componentProps: {
        ciclo: oferta.ciclo,
        descripcion: oferta.descripcion,
        fechamax: oferta.fechamax,
        numcandidatos: oferta.numcandidatos,
        requisitos: oferta.requisitos,
        titular: oferta.titular,
        id: oferta.id
      }
    }).then((modal) => modal.present());
  }

  applyOffer(oferta) {

  }
}
