import { Component, OnInit } from '@angular/core';
import { OffersService } from '../../services/offers.service';
import { AuthService } from '../../services/auth.service';
import { Offers } from '../../models/offers';

@Component({
  selector: 'app-user-offers',
  templateUrl: './user-offers.page.html',
  styleUrls: ['./user-offers.page.scss'],
})
export class UserOffersPage implements OnInit {
  ofertas: Offers[] = [];

  constructor(private offerService: OffersService,
              private authService: AuthService) {}

  ngOnInit() {
    this.offerService.getOfertas().subscribe(offers => {
      this.ofertas = offers;
    });
  }

  onLogout() {
    this.authService.logout();
  }

}
