import { Component, OnInit, Input } from '@angular/core';
import { OffersService } from '../../services/offers.service';
import { Offers } from '../../models/offers';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.scss'],
})
export class OfertasComponent implements OnInit {
  @Input() ofertas: Offers[] = [];

  constructor() { }

  ngOnInit() {}

}
