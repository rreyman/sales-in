import { Component, OnInit } from '@angular/core';
import { Users } from '../../models/users';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  confPassword = '';

  newUser: Users = {
    actived: false,
    cicle: '',
    dni: '',
    id: '',
    email: '',
    name: '',
    password: '',
    surname: '',
    type: 'user',
  };

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  }

  OnSubmitRegister() {
    this.authService.registerNewUser(this.newUser)
      .then(res => {
        // this.router.navigate(['/login']);
      }).catch(err => alert('Los datos son incorrectos o ya existen'));
  }

  // OnSubmitRegister() {
  //   this.authService.activate(this.newUser.email, this.newUser.password)
  //     .then(res => {
  //       this.router.navigate(['/login']);
  //     }).catch(err => alert('Los datos son incorrectos o ya existen'));
  // }

}
