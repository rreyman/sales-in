import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserAppliesPageRoutingModule } from './user-applies-routing.module';

import { UserAppliesPage } from './user-applies.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserAppliesPageRoutingModule
  ],
  declarations: [UserAppliesPage]
})
export class UserAppliesPageModule {}
