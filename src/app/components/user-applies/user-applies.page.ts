import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-user-applies',
  templateUrl: './user-applies.page.html',
  styleUrls: ['./user-applies.page.scss'],
})
export class UserAppliesPage implements OnInit {

  constructor(private authService: AuthService) {}

  ngOnInit() {
  }

  onLogout() {
    this.authService.logout();
  }
}
