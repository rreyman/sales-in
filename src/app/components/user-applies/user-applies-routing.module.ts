import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserAppliesPage } from './user-applies.page';

const routes: Routes = [
  {
    path: '',
    component: UserAppliesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserAppliesPageRoutingModule {}
