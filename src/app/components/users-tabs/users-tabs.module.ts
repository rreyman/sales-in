import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsersTabsPageRoutingModule } from './users-tabs-routing.module';

import { UsersTabsPage } from './users-tabs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsersTabsPageRoutingModule
  ],
  declarations: [UsersTabsPage]
})
export class UsersTabsPageModule {}
