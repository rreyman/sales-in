import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersTabsPage } from './users-tabs.page';

const routes: Routes = [
  {
    path: 'user-tabs',
    component: UsersTabsPage,
    children: [
      {
        path: 'offers',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../user-offers/user-offers.module').then(m => m.UserOffersPageModule)
          }
        ]
      },
      {
        path: 'applies',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../user-applies/user-applies.module').then(m => m.UserAppliesPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'user-tabs/offers',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'user-tabs/offers',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersTabsPageRoutingModule {}
